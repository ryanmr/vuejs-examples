Vue.js for JavaScriptMN
=======================

Tools used here:
- Foundation 6.2.0
- Vue.js 1.0.16


Sections
--------

This *lightning talk* is broken into sections.

Presentation Instructions
----

These are mostly for myself, so I can remember what needs to be done for setup.

1. clone directory
2. start `screen -S vuejsmn`
3. start a screen for `php -S localhost:xyxy`
3. prepare a screen for each directory
4. party
