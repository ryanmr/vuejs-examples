
/**
 * Example 1.
 */

var example1 = new Vue({
  el: '#example-1',

  data: {
    name: '',
    age: null
  }

});

/**
 * Example 2.
 */

var example2 = new Vue({
  el: '#example-2',

  data: {
    todo: '',
    todos: []
  },

  methods: {

    addTodo: function() {
      var todo = this.todo;
      if (todo.trim() != '') {
        this.todos.push(todo);
        this.todo = '';
      }
    },

    removeTodo: function(todo) {
      this.todos.$remove(todo);
    },

    magicTodo: function() {
      var choices = [
          'take out trash',
          'wash dishes',
          'walk the dog',
          'buy milk',
          'go to jsmn'
      ];
      var choice = choices[Math.floor(Math.random()*choices.length)];
      this.todos.push(choice);
    }

  }
});

var example3 = new Vue({
  el: '#example-3',

  data: {
    form: {
      input: ''
    },
  },

  methods: {

    submitForm: function() {
      console.log('test');
    }

  }

});
